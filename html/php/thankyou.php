<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Josol Photography</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="styles.css"/>
	<link href="css/lightbox.css" rel="stylesheet" />
	<link rel="shortcut icon" href="images/favicon.ico">
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<!-- Fixed navbar -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="http://josolphotography.me">Josol Photography</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="http://josolphotography.me">Home</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Places<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="http://josolphotography.me/biloxi">Biloxi, MS</a></li>
						<li><a href="http://josolphotography.me/covington">Covington, LA</a></li>
						<li><a href="http://josolphotography.me/dallas">Dallas, TX</a></li>
						<li><a href="http://josolphotography.me/denver">Denver, CO</a></li>
						<li><a href="http://josolphotography.me/hattiesburg">Hattiesburg, MS</a></li>
						<li><a href="http://josolphotography.me/lasvegas">Las Vegas, NV</a></li>
						<li><a href="http://josolphotography.me/neworleans">New Orleans, LA</a></li>
						<li><a href="http://josolphotography.me/sandiego">San Diego, CA</a></li>
						<li><a href="http://josolphotography.me/saucier">Saucier, MS</a></li>
					</ul>
				</li>
				<li><a href="http://josolphotography.me/map">Map</a></li>
				<li><a href="http://josolphotography.me/inspiration">Inspiration</a></li>
				<li><a href="http://josolphotography.me/gear">Gear</a></li>
				<li class="active"><a href="http://josolphotography.me/contact">Contact Me</a></li>	
			</ul>
		</div><!-- /.nav-collapse -->
	</nav>

	<div class="pageContent">
		<div class="contactForm">
			<h3>Thank you.  Your message has been sent.</h3>
		</div>
	</div>
	
	<div class="footer"></div>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<!-- collagePlus plugins -->
		<script src="js/jquery.collagePlus.min.js"></script>
		<script src="js/jquery.removeWhitespace.min.js"></script>
		<script src="js/jquery.collageCaption.min.js"></script>
	<!-- lightbox js plugin -->
	<script src="js/lightbox.min.js"></script>
	<!-- D3 js plugin -->
	<script src="js/d3.min.js"></script>
	<!-- Own js file -->
  </body>
</html>