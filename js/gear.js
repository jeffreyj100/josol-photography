(function() {
	// Image zoom slider 
	$( "#slider-range-min" ).slider({
      range: "min",
      value: 5,
      min: 5,
      max: 250,
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.value + " mm" );
		$("#zoomSample").css("transform", "scale("+ (ui.value + 50) / 50 + ")");
      }
    });
	
    $( "#amount" ).val($( "#slider-range-min" ).slider( "value" ) );
	
	// Adjust zoom slider size
	function adjustStyle(width) {
		width = parseInt(width);
		if(width < 700) {
			$("#zoomSample").css("width", "100%");
		}
		else {
			$("#zoomSample").css("width", "600px");
		}
	}
	
	adjustStyle($(this).width());
	$(window).resize(function() {
		adjustStyle($(this).width());
	});
}());