'use strict';

(function() {
	var screenWidth = $(window).width();
	
	if(screenWidth < 900) {
		$(".svgContainer").css("display", "none");
	}
	
	var screenWidth = 500;
	
	// --------------------- (D3) ---------------------------
	var margin = {top: 200, right: 5, bottom: 25, left: 40},
		width = screenWidth - margin.left - margin.right,
		height = 500 - margin.top - margin.bottom;

	// Setup x
	var xValue = function(d) { return d.ISO; },	// data -> value
		xScale = d3.scale.linear().range([25, width]),	// value -> display
		xMap = function(d) { return xScale(xValue(d)); },	// data -> display
		xAxis = d3.svg.axis().scale(xScale).orient("bottom");
		
	// Setup y
	var yValue = function(d) { return d.ShutterSpeed; },	// data -> value
		yScale = d3.scale.linear().range([height - 25, 0]),	// value -> display
		yMap = function(d) { return yScale(yValue(d)); },	// data -> display
		yAxis = d3.svg.axis().scale(yScale).orient("left");
		
	// Setup picture number
	var	numValue = function(d) { return d.Number; };
		
	// Add graph canvas to body of webpage
	var svg = d3.select(".svgContainer").append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
	// Add tooltip area to webpage
	var tooltip = d3.select(".svgContainer").append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);

	// Load data
	d3.csv("csv/denverData.csv", function(error, data) {
		// Change string (from CSV) into number format
		data.forEach(function(d) {
			d.ISO = +d.ISO;
			d.ShutterSpeed = +d.ShutterSpeed;
			d.Number = +d.Number
		});
		
		// Don't allow dots to overlap axis, so add buffer to data domain
		xScale.domain([d3.min(data, xValue) - 1, d3.max(data, xValue) + 1]);
		yScale.domain([d3.min(data, yValue) - 1, d3.max(data, yValue) + 1]);
		
		// X-axis
		svg.append("g")
		   .attr("class", "x axis")
		   .attr("transform", "translate(0," + height + ")")
		   .call(xAxis)
		   .append("text")
		   .attr("class", "label")
		   .attr("x", width)
		   .attr("y", -6)
		   .style("text-anchor", "end")
		   .text("ISO");
		   
		// Y-axis
		svg.append("g")
		   .attr("class", "y axis")
		   .call(yAxis)
		   .append("text")
		   .attr("class", "label")
		   .attr("transform", "rotate(-90)")
		   .attr("y", 6)
		   .attr("dy", ".71em")
		   .style("text-anchor", "end")
		   .text("Shutter Speed");
		   
		// Draw dots
		svg.selectAll(".dot")
		   .data(data)
		   .enter().append("circle")
		   .attr("class", "dot")
		   .style("fill", "#222222")
		   .attr("r", 6)
		   .attr("cx", xMap)
		   .attr("cy", yMap)
		   .on("mouseover", function(d) {
				// Change dot color when hovered
				d3.select(this).style("fill", "red");
			   
				tooltip.transition(200)
				.style("opacity", .9);
				
				// Display different picture
				var picNum = numValue(d);
				var fullUrl;
				if(picNum == 1) {
					fullUrl = '<img src="images/denver/min/denver001.jpg" alt=""/>';
				}
				else if(picNum == 2) {
					fullUrl = '<img src="images/denver/min/denver002.jpg" alt=""/>';
				}
				else if(picNum == 3) {
					fullUrl = '<img src="images/denver/min/denver003.jpg" alt=""/>';
				}
				else if(picNum == 4) {
					fullUrl = '<img src="images/denver/min/denver004.jpg" alt=""/>';
				}
				else if(picNum == 5) {
					fullUrl = '<img src="images/denver/min/denver004.jpg" alt=""/>';
				}
				else if(picNum == 6) {
					fullUrl = '<img src="images/denver/min/denver004.jpg" alt=""/>';
				}
				tooltip.html(fullUrl + "ISO: " + xValue(d) + ", Shutter Speed: 1/" + yValue(d))
				.style("left", (d3.event.pageX + 20) + "px")
				.style("top", (d3.event.pageY - 200) + "px");
		   })
		   .on("mouseout", function(d) {
			   // Change dot color back when hovered
				d3.select(this).style("fill", "#222222");
			   
			   tooltip.transition() 
					  .duration(500)
					  .style("opacity", 0);
		   });
	});
}());

