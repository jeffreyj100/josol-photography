// Wait for whole window to load
$(window).load(function () {
	$(document).ready(function() {
		collage();
		$('.Collage').collageCaption();
	});
});

// Apply CollagePlus plugin
function collage() {
	$('.Collage').removeWhitespace().collagePlus({
		'fadeSpeed': 2000,
		'targetHeight': 200,
		//'effect': 'effect-1' 			// <-- Continue coding.....Change transition effects
	});
};

// When browser window is resized
var resizeTimer = null;
$(window).bind('resize', function() {
	// Hide all images until we resize them
	$('.Collage img').css("opacity", 0);

	// Set timer to re-apply plugin
	if(resizeTimer) {
		clearTimeout(resizeTimer);
	}

	resizeTimer = setTimeout(collage, 200);
});
