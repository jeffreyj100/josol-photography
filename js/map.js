'use strict';

// Adjust options menu margin according to display size
function adjustStyle(width) {
	width = parseInt(width);
	if(width < 750) {
		$("#mapOptionsPanel").css("margin", "65px auto");
	}
	else {
		$("#mapOptionsPanel").css("margin", "110px auto");
	}
}

adjustStyle($(this).width());
$(window).resize(function() {
	adjustStyle($(this).width());
});

(function() {
    var config = require('../config');
	var map;
	var markers = [];
	var markersInfo = [];
	var layers = [];
	var geocoder = new google.maps.Geocoder();
	var initialLocation;
	var message = document.getElementById("notFoundMessage");
	var infoWindowGeo = new google.maps.InfoWindow({map: map});
	var curPos;
	var specLoc;
	var cityGlobal;
	var stateGlobal;
	var address;

	// ------------- (Location Info) ----------------
	var denver = {lat: 39.7392358, lng: -104.990251};
	var biloxi = {lat: 30.3960318, lng: -88.88530779999996};
	var covington = {lat: 30.4754702, lng: -90.10091080000001};
	var dallas = {lat: 32.7766642, lng: -96.79698789999998};
	var hattiesburg = {lat: 31.3271189, lng: -89.2903392};
	var lasvegas = {lat: 36.1699412, lng: -115.13982959999998};
	var neworleans = { lat: 29.95106579999999, lng: -90.0715323};
	var sandiego = {lat: 32.715738, lng: -117.16108380000003};
	var saucier = {lat: 30.6357481, lng: -89.13504490000003};

	// ----------- (InfoWindow) --------------
	var denverInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>Denver</h1>' +
		'<div id="bodyContent">' +
		'<p><b>Denver</b> is the capital and most populous municipality of the U.S. state of Colorado. As of 2014, <b>Denver</b> is also the most populous county in Colorado. <b>Denver</b> is located in the South Platte River Valley on the western edge of the High Plains just east of the Front Range of the Rocky Mountains. The <b>Denver</b> downtown district is located immediately east of the confluence of Cherry Creek with the South Platte River, approximately 12 mi (19 km) east of the foothills of the Rocky Mountains. <b>Denver</b> is nicknamed the Mile-High City because its official elevation is exactly one mile (5,280 ft or 1,610 m) above sea level, making it one of the highest major cities in the United States. The 105th meridian west of Greenwich, the longitudinal reference for the Mountain Time Zone, passes directly through Denver Union Station.</p>' +
		'<p>Attribution: Denver, <a href="https://en.wikipedia.org/wiki/Denver">' +
		'https://en.wikipedia.org/wiki/Denver</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/denver">My photos in Denver, CO</p>' +
		'</div>' +
		'</div>';

	var biloxiInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>Biloxi</h1>' +
		'<div id="bodyContent">' +
		'<p><b>Biloxi</b>, officially the City of <b>Biloxi</b>, is a city in Harrison County, Mississippi. The 2010 United States Census recorded the population as 44,054. Along with the adjoining city of Gulfport, <b>Biloxi</b> is a county seat of Harrison County. The city is part of the Gulfport-Biloxi metropolitan area and the Gulfport-Biloxi-Pascagoula, Mississippi Combined Statistical Area. Pre-Katrina, <b>Biloxi</b> was the third largest city in Mississippi behind Jackson and Gulfport; with its population losses following that storm, it was passed by Hattiesburg and Southaven, so <b>Biloxi</b> is now ranked fifth in the state. The beachfront of <b>Biloxi</b> lies directly on the Mississippi Sound, with barrier islands scattered off the coast and into the Gulf of Mexico. Keesler Air Force Base lies within the city and is home to the 81st Training Wing and the 403d Wing of the U.S. Air Force Reserve.</p>' +
		'<p>Attribution: Biloxi, <a href="https://en.wikipedia.org/wiki/Biloxi,_Mississippi">' +
		'https://en.wikipedia.org/wiki/Biloxi,_Mississippi</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/biloxi">My photos in Biloxi, MS</p>' +
		'</div>' +
		'</div>';

	var covingtonInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>Covington</h1>' +
		'<div id="bodyContent">' +
		'<p><b>Covington</b> is a city in and the parish seat of St. Tammany Parish, Louisiana, United States. The population was 8,765 at the 2010 census. It is located at a fork of the Bogue Falaya and the Tchefuncte River. <b>Covington</b> is part of the New Orleans–Metairie–Kenner Metropolitan Statistical Area. In the late 20th century, with the expansion of Louisiana\'s road system, many people who worked in New Orleans started living in <b>Covington</b>, commuting to work via the Lake Pontchartrain Causeway.</p>' +
		'<p>Attribution: Covington, <a href="https://en.wikipedia.org/wiki/Covington,_Louisiana">' +
		'https://en.wikipedia.org/wiki/Covington,_Louisiana</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/covington">My photos in Covington, LA</p>' +
		'</div>' +
		'</div>';

	var dallasInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>Dallas</h1>' +
		'<div id="bodyContent">' +
		'<p><b>Dallas</b> is a major city in Texas and is the largest urban center of the fourth most populous metropolitan area in the United States. The city proper ranks ninth in the U.S. and third in Texas after Houston and San Antonio. The city\'s prominence arose from its historical importance as a center for the oil and cotton industries, and its position along numerous railroad lines. According to the 2010 United States Census, the city had a population of 1,197,816. The United States Census Bureau\'s estimate for the city\'s population increased to 1,281,047, as of 2014. The city is the largest economic center of the 12-county Dallas–Fort Worth–Arlington metropolitan area (commonly referred to as DFW), which had a population of 6,954,330 as of July 1, 2014, representing growth in excess of 528,000 people since the 2010 census. The metropolitan economy is the sixth largest in the United States, with a 2013 real GDP of $448 billion. The city\'s economy is primarily based on banking, commerce, telecommunications, computer technology, energy, healthcare and medical research, and transportation and logistics. The city is home to the third-largest concentration of Fortune 500 companies in the nation.</p>' +
		'<p>Attribution: Dallas, <a href="https://en.wikipedia.org/wiki/Dallas">' +
		'https://en.wikipedia.org/wiki/Dallas</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/dallas">My photos in Dallas, TX</p>' +
		'</div>' +
		'</div>';

	var hattiesburgInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>Hattiesburg</h1>' +
		'<div id="bodyContent">' +
		'<p><b>Hattiesburg</b> is a city in Mississippi, bisected by the county line between Forrest County (where it is the county seat) and Lamar County. The population was 47,556 in 2013. It is the principal city of the <b>Hattiesburg</b>, Mississippi, Metropolitan Statistical Area which encompasses Forrest, Lamar and Perry counties. Founded in 1882 by civil engineer William H. Hardy, <b>Hattiesburg</b> was named in honor of Hardy\'s wife Hattie. The town was incorporated two years later with a population of 400. <b>Hattiesburg\'s</b> population first expanded as a center of the lumber and railroad industries, from which was derived the nickname "The Hub City." It now attracts newcomers to the area because of the diversity of the economy, strong neighborhoods and the central location in South Mississippi. <b>Hattiesburg</b> is home to The University of Southern Mississippi (originally known as Mississippi Normal College) and William Carey University (formerly William Carey College). South of <b>Hattiesburg</b> is Camp Shelby, the largest National Guard training base east of the Mississippi River.</p>' +
		'<p>Attribution: Hattiesburg, <a href="https://en.wikipedia.org/wiki/Hattiesburg,_Mississippi">' +
		'https://en.wikipedia.org/wiki/Hattiesburg,_Mississippi</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/hattiesburg">My photos in Hattiesburg, MS</p>' +
		'</div>' +
		'</div>';

	var lasvegasInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>Las Vegas</h1>' +
		'<div id="bodyContent">' +
		'<p><b>Las Vegas</b>, officially the City of <b>Las Vegas</b> and often known as simply Vegas, is a city in the United States, the most populous city in the state of Nevada, the county seat of Clark County, and the city proper of the Las Vegas Valley. <b>Las Vegas</b> is an internationally renowned major resort city known primarily for gambling, shopping, fine dining and nightlife and is the leading financial and cultural center for Southern Nevada. The city bills itself as The Entertainment Capital of the World, and is famous for its mega casino–hotels and associated entertainment.</p>' +
		'<p>Attribution: Las Vegas, <a href="https://en.wikipedia.org/wiki/Las_Vegas">' +
		'https://en.wikipedia.org/wiki/Las_Vegas</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/lasvegas">My photos in Las Vegas, NV</p>' +
		'</div>' +
		'</div>';

	var neworleansInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>New Orleans</h1>' +
		'<div id="bodyContent">' +
		'<p><b>New Orleans</b> is a major United States port and the largest city and metropolitan area in the state of Louisiana. The population of the city was 343,829 as of the 2010 U.S. Census. The <b>New Orleans</b> metropolitan area (New Orleans–Metairie–Kenner Metropolitan Statistical Area) had a population of 1,167,764 in 2010 and was the 46th largest in the United States. The New Orleans–Metairie–Bogalusa Combined Statistical Area, a larger trading area, had a 2010 population of 1,452,502. The city is named after the Duke of Orleans, who reigned as Regent for Louis XV from 1715 to 1723, as it was established by French colonists and strongly influenced by their European culture. It is well known for its distinct French and Spanish Creole architecture, as well as its cross-cultural and multilingual heritage. <b>New Orleans</b> is also famous for its cuisine, music (particularly as the birthplace of jazz), and its annual celebrations and festivals, most notably Mardi Gras, dating to French colonial times.</p>' +
		'<p>Attribution: New Orleans, <a href="https://en.wikipedia.org/wiki/Las_Vegas">' +
		'https://en.wikipedia.org/wiki/New_Orleans</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/neworleans">My photos in New Orleans, LA</p>' +
		'</div>' +
		'</div>';

	var sandiegoInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>San Diego</h1>' +
		'<div id="bodyContent">' +
		'<p><b>San Diego</b> is a major city in California, on the coast of the Pacific Ocean in Southern California, approximately 120 miles (190 km) south of Los Angeles and immediately adjacent to the border with Mexico. With an estimated population of 1,381,069 as of July 1, 2014, <b>San Diego</b> is the eighth-largest city in the United States and second-largest in California. <b>San Diego</b> is the birthplace of California and is known for its mild year-round climate, natural deep-water harbor, extensive beaches, long association with the U.S. Navy, and recent emergence as a healthcare and biotechnology development center.</p>' +
		'<p>Attribution: San Diego, <a href="https://en.wikipedia.org/wiki/San_Diego">' +
		'https://en.wikipedia.org/wiki/San_Diego</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/sandiego">My photos in San Diego, CA</p>' +
		'</div>' +
		'</div>';

	// Saucier
	var saucierInfo = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h1>Saucier</h1>' +
		'<div id="bodyContent">' +
		'<p><b>Saucier</b> is a census-designated place (CDP) in Harrison County, Mississippi, United States. It is part of the Gulfport–Biloxi Metropolitan Statistical Area. The population was 1,342 at the 2010 census.</p>' +
		'<p>Attribution: Saucier, <a href="https://en.wikipedia.org/wiki/Saucier,_Mississippi">' +
		'https://en.wikipedia.org/wiki/Saucier,_Mississippi</a>' + '</br></br>' +
		'<a href="http://josolphotography.me/saucier">My photos in Saucier, MS</p>' +
		'</div>' +
		'</div>';

	function addMarker(location, contentString, titleName) {
		var infoWindow = new google.maps.InfoWindow({
			content: contentString
		});
		markersInfo.push(infoWindow);

		var marker = new google.maps.Marker({
			position: location,
			map: map,
			title: titleName
		});
		markers.push(marker);

		google.maps.event.addListener(marker, 'click', function() {
			infoWindow.open(map, marker);
		});
	}

	function setMapOnAll(map) {
		for(var i = 0; i < markers.length; i++) {
			markers[i].setMap(map);
		}
	}

	function clearMarkers() {
		setMapOnAll(null);
	}

	function showMarkers() {
		setMapOnAll(map);
	}

	function deleteMarkers() {
		clearMarkers();
	}

	$("#searchCityButton").click(function() {
		$("#searchButtons").hide();
		$(".searchCityForm").show();
	});
	
	$("#searchCurrentButton").click(function() {
		$("#searchButtons").hide();
		$(".searchCurrentForm").show();
	});

	// Initialize map
	function initializeMap() {
		addMarker(denver, denverInfo, "Denver, CO");
		addMarker(biloxi, biloxiInfo, "Biloxi, MS");
		addMarker(covington, covingtonInfo, "Covington, MS");
		addMarker(dallas, dallasInfo, "Dallas, TX");
		addMarker(hattiesburg, hattiesburgInfo, "Hattiesburg, MS");
		addMarker(lasvegas, lasvegasInfo, "Las Vegas, NV");
		addMarker(neworleans, neworleansInfo, "New Orleans, LA");
		addMarker(sandiego, sandiegoInfo, "San Diego, CA");
		addMarker(saucier, saucierInfo, "Saucier, MS");
	}

	map = new google.maps.Map(document.getElementById("map-canvas"), {
		zoom: 4,
		center: denver
	});

	// Load map
	google.maps.event.addDomListener(window, 'load', initializeMap);

	var toggleKml = function(layer) {
		if(layer.getMap() == null) {
			layer.setMap(null);
		}
		else {
			layer.setMap(map);
		}
	}
	
	// Adjust zoom slider size
	function adjustStyle(width) {
		width = parseInt(width);
		if(width < 700) {
			$("#zoomSample").css("width", "100%");
		}
		else {
			$("#zoomSample").css("width", "600px");
		}
	}
	
	adjustStyle($(this).width());
	$(window).resize(function() {
		adjustStyle($(this).width());
	});
	
	var reverseGeocoder = new google.maps.Geocoder;
	var currentAddress;
	
	// Geolocation
	// Try HTML5 geolocation.
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};

			var latLngInfo = pos.lat + "," + pos.lng;
			
			// Transform lat/lng into address
			function geocodeLatLng(callback) {
				var latlngStr = latLngInfo.split(',', 2);
				var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
				reverseGeocoder.geocode({'location': latlng}, function(results, status) {
					if (status === google.maps.GeocoderStatus.OK) {
						if (results[1]) {
							callback(results[1].formatted_address);
						} else {
							window.alert('No results found');
						}
					} else {
						window.alert('Geocoder failed due to: ' + status);
					}
				});
			}
			
			geocodeLatLng(function(currentAddress) {
				var address = currentAddress;
				var locationText = "<p><b>Your location:</b> <em>" + currentAddress + "</em></p>";
				$("#locationText").append(locationText);
				
				// Search by current location
				document.getElementById('submitCurrLoc').addEventListener('click', function() {
					clearMarkers();

					geocodeFromAddress(address, map);
				});
			});
		});
	}

	// Search by city
	document.getElementById('submitSearch').addEventListener('click', function() {
		address = document.getElementById('address').value;

		clearMarkers();

		geocodeFromAddress(address, map);
	});
	
	

	function flickrFeed() {
		// Flickr API Photo Feed
		var layer;
		var urlString = "https://www.flickr.com/services/feeds/geo/United+States/state/city?format=feed-georss";
		urlString = urlString.replace(/state/, state);
		urlString = urlString.replace(/city/, city);

		layer = new google.maps.KmlLayer({
			url: urlString
		});

		return layer;
	}
	
	var city,
		state;

	function geocodeFromAddress(address, resultsMap) {
		geocoder.geocode({'address': address}, function(results, status) {
			// Locaton found
			if(status === google.maps.GeocoderStatus.OK) {
				// Collpase #mapOptions
				$("#mapOptions").collapse('hide');
				
				map.setCenter(results[0].geometry.location);
				map.setZoom(11);
				city = results[0].address_components[0].short_name;
				state = results[0].address_components[2].short_name;

				message.innerHTML = "";
				var locationFound = results[0].geometry.location;
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location
				});

				// Flickr Layer
				layers[0] = flickrFeed();
				if(document.getElementById("cityPhoto").checked == true || 
					document.getElementById("currLocPhoto").checked == true) {
					layers[0].setMap(map);
				}
				else {
					//alert("checked = false");
					layers[0].setMap(null);
				}
				//toggleKml(layers[0]);

				// ---------------------- (Traffic Layer) ----------------------------
				layers[1] = new google.maps.TrafficLayer();

				if(document.getElementById('cityTraffic').checked == true ||
					document.getElementById('currLocTraffic').checked == true) {
					layers[1].setMap(map);
				}
				else {
					layers[1].setMap(null);
				}

				// ---------------------- (Places Layer) -----------------------------
				var infowindow = new google.maps.InfoWindow();

				if(document.getElementById('cityInterest').checked == true ||
					document.getElementById('currLocInterest').checked == true) {
					var service = new google.maps.places.PlacesService(map);
					service.nearbySearch({
						location: locationFound,
						radius: 25000,
						types: ['establishment']
					}, callback);

					//noinspection JSAnnotator
                    function callback(results, status) {
						if(status === google.maps.places.PlacesServiceStatus.OK) {
							for(var i = 0; i < results.length; i++) {
								createMarker(results[i]);
							}
						}
					}

					//noinspection JSAnnotator
                    function createMarker(place) {
						var placeLoc = place.geometry.location;

						var image = {
							url: place.icon,
							size: new google.maps.Size(50, 50),
							origin: new google.maps.Point(0, 0),
							anchor: new google.maps.Point(17, 34),
							scaledSize: new google.maps.Size(20, 20)
						};

						var marker = new google.maps.Marker({
							map: map,
							icon: image,
							title: place.name,
							position: place.geometry.location
						});

						google.maps.event.addListener(marker, 'click', function() {
							infowindow.setContent(place.name);
							infowindow.open(map, this);
						});
					}
				}

				// -------------------- (Most Photographed Layer) -----------------------
				var myFlickrApiKey = config.flickrApiKey;
				var instagramAccessToken = config.instagramAccessToken;
				var heatmap;
				var lat1 = results[0].geometry.location.lat();
				var lng1 = results[0].geometry.location.lng();

				if(document.getElementById('cityHeatmap').checked == true ||
					document.getElementById('currLocHeatmap').checked == true) {
					//noinspection JSAnnotator
                    function addHeatMap(heatMapData) {
						heatmap = new google.maps.visualization.HeatmapLayer({
							data: heatMapData,
							radius: 15,
							gradient: [
									'rgba(0, 255, 255, 0)',
									'rgba(0, 255, 255, 1)',
									'rgba(0, 191, 255, 1)',
									'rgba(0, 127, 255, 1)',
									'rgba(0, 63, 255, 1)',
									'rgba(0, 0, 255, 1)',
									'rgba(0, 0, 223, 1)',
									'rgba(0, 0, 191, 1)',
									'rgba(0, 0, 159, 1)',
									'rgba(0, 0, 127, 1)',
									'rgba(63, 0, 91, 1)',
									'rgba(127, 0, 63, 1)',
									'rgba(191, 0, 31, 1)',
									'rgba(255, 0, 0, 1)'
								],
							map: map
						});

						heatmap.setMap(map);
					}

					// Flickr heatmap data
					//noinspection JSAnnotator
                    function showFlickrHeatMap(flickrApiKey) {
						$.get('https://www.flickr.com/services/rest/?' +
						'method=flickr.photos.search&api_key=' + flickrApiKey +
						'&lat=' + lat1 + '&lon=' +
						lng1 + '&radius=30' + '&extras=geo&format=json&per_page=5000&nojsoncallback=1')
						.done(function (data) {
							// Gather the locations of each of the photos Flickr gives
							var heatMapData = [];

							if(data.photos && data.photos.photo) {
								for(var i = 0; i < data.photos.photo.length; i++) {
									heatMapData.push(
										new google.maps.LatLng(data.photos.photo[i].latitude, data.photos.photo[i].longitude)
									);
								}
							}

							addHeatMap(heatMapData);
						});
					}

					// Instagram heatmap data
					//noinspection JSAnnotator
                    function showInstagramHeatMap(instagramAccessToken) {
						$.get('https://api.instagram.com/v1/media/search?' +
							'lat=' + lat1 + '&lng=' + lng1 + '&distance=5000' +
							'&access_token=' + instagramAccessToken,

						function () {}, 'jsonp').done(function (data) {
							// Gather the locations of each of the photos Instagram gives us
							var heatMapData = [];

							if (data.data) {
								for (var i = 0; i < data.data.length; i += 1) {
									heatMapData.push(
										new google.maps.LatLng(data.data[i].location.latitude, data.data[i].location.longitude)
									);
								}
							}

							addHeatMap(heatMapData);
						});
					}

					showInstagramHeatMap(instagramAccessToken);
					showFlickrHeatMap(myFlickrApiKey);
				}

				// Weather Layer
				var openWeatherMapKey = config.openWeatherMapKey;
				var request;
				var geoJSON;
				var gettingData = false;
				var infowindowWeather = new google.maps.InfoWindow();

				// Sets up and populates the info window with details
				map.data.addListener('click', function(event) {
				  infowindowWeather.setContent(
				   "<img src=" + event.feature.getProperty("icon") + ">"
				   + "<br /><strong>" + event.feature.getProperty("city") + "</strong>"
				   + "<br />" + event.feature.getProperty("temperature") + "&deg;F"
				   + "<br />" + event.feature.getProperty("weather")
				   );
				  infowindowWeather.setOptions({
					  position:{
						lat: event.latLng.lat(),
						lng: event.latLng.lng()
					  },
					  pixelOffset: {
						width: 0,
						height: -15
					  }
					});
				  infowindowWeather.open(map);
				});

				var checkIfDataRequested = function() {
					// Stop extra requests being sent
					while (gettingData === true) {
						request.abort();
						gettingData = false;
					}

					getCoords();
				};

				// Get the coordinates from the Map bounds
				var getCoords = function() {
					var bounds = map.getBounds();
					var NE = bounds.getNorthEast();
					var SW = bounds.getSouthWest();
					getWeather(NE.lat(), NE.lng(), SW.lat(), SW.lng());
				};

				// Make the weather request
				var getWeather = function(northLat, eastLng, southLat, westLng) {
					gettingData = true;

					var requestString = "http://api.openweathermap.org/data/2.5/box/city?bbox="
					+ westLng + "," + northLat + "," //left top
					+ eastLng + "," + southLat + "," //right bottom
					+ map.getZoom()
					+ "&cluster=yes&format=json"
					+ "&APPID=" + openWeatherMapKey
					+ "&units=imperial";

					request = new XMLHttpRequest();
					request.onload = proccessResults;
					request.open("get", requestString, true);
					request.send();
				};

				// Take the JSON results and proccess them
				var proccessResults = function() {
					var results = JSON.parse(this.responseText);

					if (results.list.length > 0) {
						resetData();

						for (var i = 0; i < results.list.length; i++) {
							geoJSON.features.push(jsonToGeoJson(results.list[i]));
						}

						drawIcons(geoJSON);
					}
				};

				// For each result that comes back, convert the data to geoJSON
				var jsonToGeoJson = function (weatherItem) {
					var feature = {
						type: "Feature",
						properties: {
							city: weatherItem.name,
							weather: weatherItem.weather[0].main,
							temperature: Math.round(weatherItem.main.temp),
							min: weatherItem.main.temp_min,
							max: weatherItem.main.temp_max,
							humidity: weatherItem.main.humidity,
							pressure: weatherItem.main.pressure,
							windSpeed: weatherItem.wind.speed,
							windDegrees: weatherItem.wind.deg,
							windGust: weatherItem.wind.gust,
							icon: "http://openweathermap.org/img/w/"
							+ weatherItem.weather[0].icon  + ".png",
							coordinates: [weatherItem.coord.lon, weatherItem.coord.lat]
						},
						geometry: {
							type: "Point",
							coordinates: [weatherItem.coord.lon, weatherItem.coord.lat]
						}
					};

					// Set the custom marker icon
					map.data.setStyle(function(feature) {
						return {
							icon: {
								url: feature.getProperty('icon'),
								anchor: new google.maps.Point(25, 25)
							}
						};
					});

					// returns object
					return feature;
				};

				// Add the markers to the map
				var drawIcons = function (weather) {
					map.data.addGeoJson(geoJSON);
					// Set the flag to finished
					gettingData = false;
				};

				// Clear data layer and geoJSON
				var resetData = function () {
					geoJSON = {
						type: "FeatureCollection",
						features: []
					};

					map.data.forEach(function(feature) {
						map.data.remove(feature);
					});
				};

				if(document.getElementById('cityWeather').checked == true ||
					document.getElementById('currLocWeather').checked == true) {
					// Add interaction listeners to make weather requests
					checkIfDataRequested();
					//google.maps.event.addListener(map, 'idle', checkIfDataRequested);
				}
				else {
					resetData();
				}
			}
			// Location not found
			else {
				message.innerHTML = "Location not found, try again.";
			}
		});
	}
}()); // End of IEFE