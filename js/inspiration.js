(function() {
    'use strict';
    var config = require('../config');

	// Layer 1
	var introTitle = "<div id='introTitle'>" +
					"<h1>Find your taste for Photography</h1>" +
					 "<br/><input type='submit' class='btn btn-default' id='beginButton' value='Begin' />" +
					 "</div>";
					 
	// Layer 2
	var pickInterest = "<div id='pickInterest'>" +
						   "<h3>Select your interests: </h3>" + 
						   //"<input type='checkbox' name='choices[]' value='Animals'/>&nbspAnimals<br />" +
						   "<div class='checkboxes'>" +
								"<input type='checkbox' name='choices[]' value='Cars'/>&nbspCars<br />" +
								"<input type='checkbox' name='choices[]' value='Nature'/>&nbspNature<br />" +
								"<input type='checkbox' name='choices[]' value='People'/>&nbspPeople<br />" +
								"<input type='checkbox' name='choices[]' value='Art'/>&nbspArt<br />" +
								"<input type='checkbox' name='choices[]' value='Architecture'/>&nbspArchitecture<br />" +
								"<input type='checkbox' name='choices[]' value='Urban'/>&nbspUrban/City<br />" +
								"<input type='checkbox' name='choices[]' value='Fashion'/>&nbspFashion<br />" +
								"<input type='checkbox' name='choices[]' value='Travel'/>&nbspTravel<br />" +
								"<input type='checkbox' name='choices[]' value='Food'/>&nbspFood<br />" +
						   "</div>" +
						   /*"<input type='checkbox' name='choices[]' value='Night'/>&nbspNight<br />" +
						   "<input type='checkbox' name='choices[]' value='Journalism'/>&nbspJournalism<br />" +
						   "<input type='checkbox' name='choices[]' value='Sports'/>&nbspSports<br />" +
						   "<input type='checkbox' name='choices[]' value='Concerts'/>&nbspConcert's<br />" +
						   "<input type='checkbox' name='choices[]' value='Wedding'/>&nbspWedding<br />" +
						   "<input type='checkbox' name='choices[]' value='Vintage'/>&nbspVintage<br />" +
						   "<input type='checkbox' name='choices[]' value='Texture'/>&nbspTexture<br />" +
						   "<input type='checkbox' name='choices[]' value='Rural'/>&nbspRural<br />" +
						   "<input type='checkbox' name='choices[]' value='Patterns'/>&nbspPatterns<br />" +*/
						   "<br/><br/><input type='submit' class='btn btn-default' id='continue1' value='Continue' />" +
					   "</div>";
					   
	// Layer 3
	var pickPhotos = "<div id='pickPhotos'>" +
						"<h3>Select the Photos you find interesting:</h3>" +
					 "</div>";
			
	// Layer 4
	var photoQuestions = "<div id='photoQuestions'>" +
						 "</div>";
						 
	// Layer 5
	var calculatedType = "<div id='calculatedType'>" +
							 "<div id='typeGallary'>" +
								 "<div id='images'>" +
								 "</div>" +
							 "</div>" +
						 "</div>";

	// Add content to app div
	// Layer 1
	$("#app").css("paddingTop", "250px");
	$("#app").append(introTitle);

	// Layer 2
	$("#app").append(pickInterest);
	$("#pickInterest").hide();

	// Layer 3
	$("#app").append(pickPhotos);
	$("#pickPhotos").hide();

	// Layer 4
	$("#app").append(photoQuestions);
	$("#photoQuestions").hide();

	// Layer 5
	$("#app").append(calculatedType);
	$("#calculatedType").hide();

	$("#beginButton").click(function() {
		// -----<<Hide Layer 1>>-------
		$("#introTitle").fadeOut(800, function() {
			$(this).remove();
			
			// -------<<Show Layer 2>>-------
			$("#app").css("paddingTop", "50px");
			$("#pickInterest").fadeIn(800, function() {
				$(this).show();
			});
		});
	});

	var numChoices = 19;
	var choices = [];
	var selection = [];

	// --------------- (Picture elements) ------------------
	// Cars
	var carsClassic = "<img src='images/inspiration/Cars/carsClassic.jpg' alt=''/></a>",
		carsExotic = "<img src='images/inspiration/Cars/carsExotic.jpg' alt=''/></a>",
		carsFuturistic = "<img src='images/inspiration/Cars/carsFuturistic.jpg' alt=''/></a>",
		carsOffRoad = "<img src='images/inspiration/Cars/carsOffRoad.jpg' alt=''/></a>";
	// Nature
	var natureDesert = "<img src='images/inspiration/Nature/natureDesert.jpg' alt=''/></a>",
		natureForrest = "<img src='images/inspiration/Nature/natureForrest.jpg' alt=''/></a>",
		natureMountain = "<img src='images/inspiration/Nature/natureMountain.jpg' alt=''/></a>",
		natureOcean = "<img src='images/inspiration/Nature/natureOcean.jpg' alt=''/></a>";
	// People
	var peopleCosplay = "<img src='images/inspiration/People/peopleCosplay.jpg' alt=''/></a>",
		peopleCrowd = "<img src='images/inspiration/People/peopleCrowd.jpg' alt=''/></a>",
		peopleDailyLife = "<img src='images/inspiration/People/peopleDailyLife.jpg' alt=''/></a>",
		peoplePortrait = "<img src='images/inspiration/People/peoplePortrait.jpg' alt=''/></a>";
	// Art
	var artAbstract = "<img src='images/inspiration/Art/artAbstract.jpg' alt=''/></a>",
		artImpressionism = "<img src='images/inspiration/Art/artImpressionism.jpg' alt=''/></a>",
		artRealism = "<img src='images/inspiration/Art/artRealism.jpg' alt=''/></a>",
		artSurrealism = "<img src='images/inspiration/Art/artSurrealism.jpg' alt=''/></a>";
	// Architecture
	var architectAncient = "<img src='images/inspiration/Architecture/architectAncient.jpg' alt=''/></a>",
		architectAsian = "<img src='images/inspiration/Architecture/architectAsian.jpg' alt=''/></a>",
		architectMidieval= "<img src='images/inspiration/Architecture/architectMidieval.jpg' alt=''/></a>",
		architectModern = "<img src='images/inspiration/Architecture/architectModern.jpg' alt=''/></a>";
	// Urban
	var urbanAtmosphere = "<img src='images/inspiration/Urban/urbanAtmosphere.jpg' alt=''/></a>",
		urbanMobility = "<img src='images/inspiration/Urban/urbanMobility.jpg' alt=''/></a>",
		urbanTech = "<img src='images/inspiration/Urban/urbanTech.jpg' alt=''/></a>",
		urbanYoung = "<img src='images/inspiration/Urban/urbanYoung.jpg' alt=''/></a>";
	// Fashion
	var fashionClassicFit = "<img src='images/inspiration/Fashion/fashionClassicFit.jpg' alt=''/></a>",
		fashionInternational = "<img src='images/inspiration/Fashion/fashionInternational.jpg' alt=''/></a>",
		fashionSophisticated = "<img src='images/inspiration/Fashion/fashionSophisticated.jpg' alt=''/></a>",
		fashionWestern = "<img src='images/inspiration/Fashion/fashionWestern.jpg' alt=''/></a>";
	// Travel
	var travelAirBallon = "<img src='images/inspiration/Travel/travelAirBallon.jpg' alt=''/></a>",
		travelAirport = "<img src='images/inspiration/Travel/travelAirport.jpg' alt=''/></a>",
		travelRoad = "<img src='images/inspiration/Travel/travelRoad.jpg' alt=''/></a>",
		travelDesert = "<img src='images/inspiration/Travel/travelDesert.jpg' alt=''/></a>";
	// Food
	var foodCupCake = "<img src='images/inspiration/Food/foodCupCake.jpg' alt=''/></a>",
		foodMarket = "<img src='images/inspiration/Food/foodMarket.jpg' alt=''/></a>",
		foodSoup = "<img src='images/inspiration/Food/foodSoup.jpg' alt=''/></a>",
		foodSteak = "<img src='images/inspiration/Food/foodSteak.jpg' alt=''/></a>";

	$("#continue1").click(function() {	
		$("input[name='choices[]']:checked").each(function() {
			choices.push($(this).val());
		});
		
		var pictures = [];
		
		for(var i = 0; i < numChoices; i++) {
			switch(choices[i]) {
				case "Cars":
					pictures.push(carsClassic, carsExotic, carsFuturistic, carsOffRoad);
					break;
				case "Nature":
					pictures.push(natureDesert, natureForrest, natureMountain, natureOcean);
					break;
				case "People":
					pictures.push(peopleCosplay, peopleCrowd, peopleDailyLife, peoplePortrait);
					break;
				case "Art":
					pictures.push(artAbstract, artImpressionism, artRealism, artSurrealism);
					break;
				case "Architecture":
					pictures.push(architectAncient, architectAsian, architectMidieval, architectModern);
					break;
				case "Urban":
					pictures.push(urbanAtmosphere, urbanMobility, urbanTech, urbanYoung);
					break;
				case "Fashion":
					pictures.push(fashionClassicFit, fashionInternational, fashionSophisticated, fashionWestern);
					break;
				case "Travel":
					pictures.push(travelAirBallon, travelAirport, travelRoad, travelDesert);
					break;
				case "Food":
					pictures.push(foodCupCake, foodMarket, foodSoup, foodSteak);
					break;
				// Possible additions
				/*case "Night":
					console.log("(TEST) Night selected");
					break;
				case "Journalism":
					console.log("(TEST) Journalism selected");
					break;
				case "Sports":
					console.log("(TEST) Sports selected");
					break;
				case "Concerts":
					console.log("(TEST) Concerts selected");
					break;
				case "Wedding":
					console.log("(TEST) Wedding selected");
					break;
				case "Vintage":
					console.log("(TEST) Vintage selected");
					break;
				case "Texture":
					console.log("(TEST) Texture selected");
					break;
				case "Rural":
					console.log("(TEST) Rural selected");
					break;
				case "Patterns":
					console.log("(TEST) Patterns selected");
					break;*/
			}
		}
		
		var contButton = "<br /><br /><br /><input type='submit' class='btn btn-default' id='continue2' value='Continue' />";
		
		for(var i = 0; i < pictures.length; i++) {
			$("#pickPhotos").append(pictures[i]);
		}
		
		$("#pickPhotos").append(contButton);
		
		// Toggle selection on photos
		$("#pickPhotos img").click(function() {
			$(this).toggleClass("border");
		});

		// -------<<Hide Layer 2>>-------
		$("#pickInterest").fadeOut(800, function() {
			$(this).remove();
			
			// -------<<Show Layer 3>>-------
			$("#pickPhotos").fadeIn(800, function() {
				$(this).show();
			});
		});
		
		// Question elements
		var questionTitle = "<p id='quesTitle'>How much do you like the photo(s)?</p><br />" +
							"<p>(1) Ok to (5) Love</p><br />" +
							"<hr>";
							
		function CreateRating(photo, name) {
			var eachRating = 
			"<div id='" + name + "'>" +
				  photo + "<br /><br />" +
				  "<label class='radio-inline'><input type='radio' name='" + name + "' value='1'>&nbsp(1)</label>" +
				  "<label class='radio-inline'><input type='radio' name='" + name + "' value='2'>&nbsp(2)</label>" +
				  "<label class='radio-inline'><input type='radio' name='" + name + "' value='3'>&nbsp(3)</label>" +
				  "<label class='radio-inline'><input type='radio' name='" + name + "' value='4'>&nbsp(4)</label>" +
				  "<label class='radio-inline'><input type='radio' name='" + name + "' value='5'>&nbsp(5)</label>" +
				  "<hr>" +
			"</div>";
			
			return eachRating;
		}
						
		// Cars
		var carsClassicQues = CreateRating(carsClassic, "carsClassicQues"),
			carsExoticQues = CreateRating(carsExotic, "carsExoticQues"),
			carsFuturisticQues = CreateRating(carsFuturistic, "carsFuturisticQues"),
			carsOffRoadQues = CreateRating(carsOffRoad, "carsOffRoadQues");
		// Nature
		var natureDesertQues = CreateRating(natureDesert, "natureDesertQues"),
			natureForrestQues = CreateRating(natureForrest, "natureForrestQues"),
			natureMountainQues = CreateRating(natureMountain, "natureMountainQues"),
			natureOceanQues = CreateRating(natureOcean, "natureOceanQues");
		// People
		var peopleCosplayQues = CreateRating(peopleCosplay, "peopleCosplayQues"),
			peopleCrowdQues = CreateRating(peopleCrowd, "peopleCrowdQues"),
			peopleDailyLifeQues = CreateRating(peopleDailyLife, "peopleDailyLifeQues"),
			peoplePortraitQues = CreateRating(peoplePortrait, "peoplePortraitQues");
		// Art
		var artAbstractQues = CreateRating(artAbstract, "artAbstractQues"),
			artImpressionismQues = CreateRating(artImpressionism, "artImpressionismQues"),
			artRealismQues = CreateRating(artRealism, "artRealismQues"),
			artSurrealismQues = CreateRating(artSurrealism, "artSurrealismQues");
		// Architecture
		var architectAncientQues = CreateRating(architectAncient, "architectAncientQues"),
			architectAsianQues = CreateRating(architectAsian, "architectAsianQues"),
			architectMidievalQues = CreateRating(architectMidieval, "architectMidievalQues"),
			architectModernQues = CreateRating(architectModern, "architectModernQues");
		// Urban
		var urbanAtmosphereQues = CreateRating(urbanAtmosphere, "urbanAtmosphereQues"),
			urbanMobilityQues = CreateRating(urbanMobility, "urbanMobilityQues"),
			urbanTechQues = CreateRating(urbanTech, "urbanTechQues"),
			urbanYoungQues = CreateRating(urbanYoung, "urbanYoungQues");
		// Fashion
		var fashionClassicFitQues = CreateRating(fashionClassicFit, "fashionClassicFitQues"),
			fashionInternationalQues = CreateRating(fashionInternational, "fashionInternationalQues"),
			fashionSophisticatedQues = CreateRating(fashionSophisticated, "fashionSophisticatedQues"),
			fashionWesternQues = CreateRating(fashionWestern, "fashionWesternQues");
		// Travel
		var travelAirBallonQues = CreateRating(travelAirBallon, "travelAirBallonQues"),
			travelAirportQues = CreateRating(travelAirport, "travelAirportQues"),
			travelRoadQues = CreateRating(travelRoad, "travelRoadQues"),
			travelDesertQues = CreateRating(travelDesert, "travelDesertQues");
		// Food
		var foodCupCakeQues = CreateRating(foodCupCake, "foodCupCakeQues"),
			foodMarketQues = CreateRating(foodMarket, "foodMarketQues"),
			foodSoupQues = CreateRating(foodSoup, "foodSoupQues"),
			foodSteakQues = CreateRating(foodSteak, "foodSteakQues");
							  
		var contButtonLayers4 = "<br /><input type='submit' class='btn btn-default' id='continue3' value='Continue' />";
		
		// Layer 4
		$("#continue2").click(function() {
			var jpgNames = [];
			var jpgName;
			
			// Check which photos have been selected
			$("#pickPhotos img").each(function() {
				if($(this).hasClass("border")) {
					selection.push(this);
				}
			});
			
			// Parse selected image object for name only
			for(var i = 0; i < selection.length; i++) {
				var photoURL = selection[i].currentSrc;
				photoURL = photoURL.substring(27);
				photoURL = photoURL.replace( /^.*?([^\/]+)\..+?$/, '$1' );
				jpgName = photoURL;
				
				jpgNames.push(jpgName);
			}
			
			$("#photoQuestions").append(questionTitle);
			
			// Display question for each selected photo
			for(var i = 0; i < jpgNames.length; i++) {			
				switch(jpgNames[i]) {
					// Cars
					case "carsClassic":
						$("#photoQuestions").append(carsClassicQues);
						break;
					case "carsExotic":
						$("#photoQuestions").append(carsExoticQues);
						break;
					case "carsFuturistic":
						$("#photoQuestions").append(carsFuturisticQues);
						break;
					case "carsOffRoad":
						$("#photoQuestions").append(carsOffRoadQues);
						break;
					// Nature
					case "natureDesert":
						$("#photoQuestions").append(natureDesertQues);
						break;
					case "natureForrest":
						$("#photoQuestions").append(natureForrestQues);
						break;
					case "natureMountain":
						$("#photoQuestions").append(natureMountainQues);
						break;
					case "natureOcean":
						$("#photoQuestions").append(natureOceanQues);
						break;
					// People
					case "peopleCosplay":
						$("#photoQuestions").append(peopleCosplayQues);
						break;
					case "peopleCrowd":
						$("#photoQuestions").append(peopleCrowdQues);	
						break;
					case "peopleDailyLife":
						$("#photoQuestions").append(peopleDailyLifeQues);	
						break;
					case "peoplePortrait":
						$("#photoQuestions").append(peoplePortraitQues);	
						break;
					// Art
					case "artAbstract":
						$("#photoQuestions").append(artAbstractQues);	
						break;
					case "artImpressionism":
						$("#photoQuestions").append(artImpressionismQues);	
						break;
					case "artRealism":
						$("#photoQuestions").append(artRealismQues);	
						break;
					case "artSurrealism":
						$("#photoQuestions").append(artSurrealismQues);
						break;
					// Architecture
					case "architectAncient":
						$("#photoQuestions").append(architectAncientQues);
						break;
					case "architectAsian":
						$("#photoQuestions").append(architectAsianQues);
						break;
					case "architectMidieval":
						$("#photoQuestions").append(architectMidievalQues);
						break;
					case "architectModern":
						$("#photoQuestions").append(architectModernQues);
						break;
					// Urban
					case "urbanAtmosphere":
						$("#photoQuestions").append(urbanAtmosphereQues);
						break;
					case "urbanMobility":
						$("#photoQuestions").append(urbanMobilityQues);
						break;
					case "urbanTech":
						$("#photoQuestions").append(urbanTechQues);
						break;
					case "urbanYoung":
						$("#photoQuestions").append(urbanYoungQues);
						break;
					// Fashion
					case "fashionClassicFit":
						$("#photoQuestions").append(fashionClassicFitQues);	
						break;
					case "fashionInternational":
						$("#photoQuestions").append(fashionInternationalQues);
						break;
					case "fashionSophisticated":
						$("#photoQuestions").append(fashionSophisticatedQues);	
						break;
					case "fashionWestern":
						$("#photoQuestions").append(fashionWesternQues);	
						break;
					// Travel
					case "travelAirBallon":
						$("#photoQuestions").append(travelAirBallonQues);
						break;
					case "travelAirport":
						$("#photoQuestions").append(travelAirportQues);
						break;
					case "travelRoad":
						$("#photoQuestions").append(travelRoadQues);
						break;
					case "travelDesert":
						$("#photoQuestions").append(travelDesertQues);
						break;
					// Food
					case "foodCupCake":
						$("#photoQuestions").append(foodCupCakeQues);
						break;
					case "foodMarket":
						$("#photoQuestions").append(foodMarketQues);
						break;
					case "foodSoup":
						$("#photoQuestions").append(foodSoupQues);
						break;
					case "foodSteak":
						$("#photoQuestions").append(foodSteakQues);
						break;
				}	
			}
			
			$("#photoQuestions").append(contButtonLayers4);
			
			// -------<<Hide Layer 3>>-------
			$("#pickPhotos").fadeOut(800, function() {
				$(this).remove();
				$('html,body').scrollTop(0);	// Scroll to top before loading Layer 4
				
				// -------<<Show Layer 4>>-------
				$("#photoQuestions").fadeIn(800, function() {
					$(this).show();
				});
			});
			
			// Like values for selected photo(s)
			var carsClassicVal, carsExoticVal, carsFuturisticVal, carsOffRoadVal;  // Cars
			var natureDesertVal, natureForrestVal, natureMountainVal, natureOceanVal;  // Nature
			var peopleCosplayVal, peopleCrowdVal, peopleDailyLifeVal, peoplePortraitVal;  // People
			var artAbstractVal, artImpressionismVal, artRealismVal, artSurrealismVal; // Art
			var architectAncientVal, architectAsianVal, architectMidievalVal, architectModernVal;  // Architecture
			var urbanAtmosphereVal, urbanMobilityVal, urbanTechVal, urbanYoungVal;  // Urban
			var fashionClassicFitVal, fashionInternationalVal, fashionSophisticatedVal, fashionWesternVal;  // Fashion
			var travelAirBallonVal, travelAirportVal, travelRoadVal, travelDesertVal;  // Travel
			var foodCupCakeVal, foodMarketVal, foodSoupVal, foodSteakVal;  // Food
			
			// Layer 5
			$("#continue3").click(function() {
				var scores = {
					fashion: {textType: "fashion", score: 0},
					blackandwhite: {textType: "black and white", score: 0},
					hdr: {textType: "HDR", score: 0},
					motion: {textType: "motion", score: 0},
					sunriseandsunset: {textType: "sunrise and sunset", score: 0},
					night: {textType: "night", score: 0},
					landscape: {textType: "landscape", score: 0},
					street: {textType: "street", score: 0},
					modeling: {textType: "modeling", score: 0},
					stillobject: {textType: "still life", score: 0},
					futuristic: {textType: "futuristic", score: 0},
					bokeh: {textType: "bokeh", score: 0},
					portrait: {textType: "portrait", score: 0},
					adventure: {textType: "adventure", score: 0},
					abstraction: {textType: "abstract", score: 0},
					pattern: {textType: "pattern", score: 0},
					life: {textType: "general", score: 0}	
				};
				
				// Score weight factor algorithm
				// Cars
				carsClassicVal = $('input[name=carsClassicQues]:checked').val();
				if (isNaN(carsClassicVal)) carsClassicVal = 0;
				carsClassicVal = parseInt(carsClassicVal);
				if(carsClassicVal > 0) {
					scores["stillobject"]["score"] += carsClassicVal;
				}
				
				carsExoticVal = $('input[name=carsExoticQues]:checked').val();
				if (isNaN(carsExoticVal)) carsExoticVal = 0;
				carsExoticVal = parseInt(carsExoticVal);
				if(carsExoticVal > 0) {
					scores["stillobject"]["score"] += carsExoticVal;
				}
				
				carsFuturisticVal = $('input[name=carsFuturisticQues]:checked').val();
				if (isNaN(carsFuturisticVal)) carsFuturisticVal = 0;
				carsFuturisticVal = parseInt(carsFuturisticVal);
				if(carsFuturisticVal > 0) {
					scores["futuristic"]["score"] += carsFuturisticVal;
					scores["stillobject"]["score"] += carsFuturisticVal - 1;
				}
				
				carsOffRoadVal = $('input[name=carsOffRoadQues]:checked').val();
				if (isNaN(carsOffRoadVal)) carsOffRoadVal = 0;
				carsOffRoadVal = parseInt(carsOffRoadVal);
				if(carsOffRoadVal > 0) {
					scores["motion"]["score"] += carsOffRoadVal;
				}
				
				// Nature
				natureDesertVal = $('input[name=natureDesertQues]:checked').val();
				if (isNaN(natureDesertVal)) natureDesertVal = 0;
				natureDesertVal = parseInt(natureDesertVal);
				if(natureDesertVal > 0) {
					scores["adventure"]["score"] += natureDesertVal;
				}
				
				natureForrestVal = $('input[name=natureForrestQues]:checked').val();
				if (isNaN(natureForrestVal)) natureForrestVal = 0;
				natureForrestVal = parseInt(natureForrestVal);
				if(natureForrestVal > 0) {
					scores["sunriseandsunset"]["score"] += natureForrestVal;
					scores["night"]["score"] += natureForrestVal - 1;
				}
				
				natureMountainVal = $('input[name=natureMountainQues]:checked').val();
				if (isNaN(natureMountainVal)) natureMountainVal = 0;
				natureMountainVal = parseInt(natureMountainVal);
				if(natureMountainVal > 0) {
					scores["landscape"]["score"] += natureMountainVal;
					scores["sunriseandsunset"]["score"] += natureMountainVal - 1;
				}
				
				natureOceanVal = $('input[name=natureOceanQues]:checked').val();
				if (isNaN(natureOceanVal)) natureOceanVal = 0;
				natureOceanVal = parseInt(natureOceanVal);
				if(natureOceanVal > 0) {
					scores["sunriseandsunset"]["score"] += natureOceanVal;
					scores["landscape"]["score"] += natureOceanVal - 1;
				}
				
				// People
				peopleCosplayVal = $('input[name=peopleCosplayQues]:checked').val();
				if (isNaN(peopleCosplayVal)) peopleCosplayVal = 0;
				peopleCosplayVal = parseInt(peopleCosplayVal);
				if(peopleCosplayVal > 0) {
					scores["modeling"]["score"] += peopleCosplayVal;
					scores["fashion"]["score"] += peopleCosplayVal - 1;
				}
				
				peopleCrowdVal = $('input[name=peopleCrowdQues]:checked').val();
				if (isNaN(peopleCrowdVal)) peopleCrowdVal = 0;
				peopleCrowdVal = parseInt(peopleCrowdVal);
				if(peopleCrowdVal > 0) {
					scores["blackandwhite"]["score"] += peopleCrowdVal;
					scores["street"]["score"] += peopleCrowdVal - 1;
				}
				
				peopleDailyLifeVal = $('input[name=peopleDailyLifeQues]:checked').val();
				if (isNaN(peopleDailyLifeVal)) peopleDailyLifeVal = 0;
				peopleDailyLifeVal = parseInt(peopleDailyLifeVal);
				if(peopleDailyLifeVal > 0) {
					scores["blackandwhite"]["score"] += peopleDailyLifeVal;
					scores["street"]["score"] += peopleDailyLifeVal - 1;
				}
				
				peoplePortraitVal = $('input[name=peoplePortraitQues]:checked').val();
				if (isNaN(peoplePortraitVal)) peoplePortraitVal = 0;
				peoplePortraitVal = parseInt(peoplePortraitVal);
				if(peoplePortraitVal > 0) {
					scores["modeling"]["score"] += peoplePortraitVal;
				}
				
				// Art
				artAbstractVal = $('input[name=artAbstractQues]:checked').val();
				if (isNaN(artAbstractVal)) artAbstractVal = 0;
				artAbstractVal = parseInt(artAbstractVal);
				if(artAbstractVal > 0) {
					scores["bokeh"]["score"] += artAbstractVal; 
				}
				
				artImpressionismVal = $('input[name=artImpressionismQues]:checked').val();
				if (isNaN(artImpressionismVal)) artImpressionismVal = 0;
				artImpressionismVal = parseInt(artImpressionismVal);
				if(artImpressionismVal > 0) {
					scores["sunriseandsunset"]["score"] += artImpressionismVal;
					scores["landscape"]["score"] += artImpressionismVal - 1;
				}
				
				artRealismVal = $('input[name=artRealismQues]:checked').val();
				if (isNaN(artRealismVal)) artRealismVal = 0;
				artRealismVal = parseInt(artRealismVal);
				if(artRealismVal > 0) {
					scores["landscape"]["score"] += artRealismVal;
				}
				
				artSurrealismVal = $('input[name=artSurrealismQues]:checked').val();
				if (isNaN(artSurrealismVal)) artSurrealismVal = 0;
				artSurrealismVal = parseInt(artSurrealismVal);
				if(artSurrealismVal > 0) {
					scores["futuristic"]["score"] += artSurrealismVal;
				}
				
				// Architecture
				architectAncientVal = $('input[name=architectAncientQues]:checked').val();
				if (isNaN(architectAncientVal)) architectAncientVal = 0;
				architectAncientVal = parseInt(architectAncientVal);
				if(architectAncientVal > 0) {
					scores["sunriseandsunset"]["score"] += architectAncientVal;
				}
				
				architectAsianVal = $('input[name=architectAsianQues]:checked').val();
				if (isNaN(architectAsianVal)) architectAsianVal = 0;
				architectAsianVal = parseInt(architectAsianVal);
				if(architectAsianVal > 0) {
					scores["adventure"]["score"] += architectAsianVal;
				}
				
				architectMidievalVal = $('input[name=architectMidievalQues]:checked').val();
				if (isNaN(architectMidievalVal)) architectMidievalVal = 0;
				architectMidievalVal = parseInt(architectMidievalVal);
				if(architectMidievalVal > 0) {
					scores["hdr"]["score"] += architectMidievalVal;
					scores["landscape"]["score"] += architectMidievalVal - 1;
				}
				
				architectModernVal = $('input[name=architectModernQues]:checked').val();
				if (isNaN(architectModernVal)) architectModernVal = 0;
				architectModernVal = parseInt(architectModernVal);
				if(architectModernVal > 0) {
					scores["futuristic"]["score"] += architectModernVal;
					scores["hdr"]["score"] += architectModernVal - 1;
				}
				
				// Urban
				urbanAtmosphereVal = $('input[name=urbanAtmosphereQues]:checked').val();
				if (isNaN(urbanAtmosphereVal)) urbanAtmosphereVal = 0;
				urbanAtmosphereVal = parseInt(urbanAtmosphereVal);
				if(urbanAtmosphereVal > 0) {
					scores["night"]["score"] += urbanAtmosphereVal;
					scores["landscape"]["score"] += urbanAtmosphereVal - 1;
				}
				
				urbanMobilityVal = $('input[name=urbanMobilityQues]:checked').val();
				if (isNaN(urbanMobilityVal)) urbanMobilityVal = 0;
				urbanMobilityVal = parseInt(urbanMobilityVal);
				if(urbanMobilityVal > 0) {
					scores["motion"]["score"] += urbanMobilityVal;
					scores["night"]["score"] += urbanMobilityVal - 1;
					scores["street"]["score"] += urbanMobilityVal - 2;
				}
				
				urbanTechVal = $('input[name=urbanTechQues]:checked').val();
				if (isNaN(urbanTechVal)) urbanTechVal = 0;
				urbanTechVal = parseInt(urbanTechVal);
				if(urbanTechVal > 0) {
					scores["night"]["score"] += urbanTechVal;
				}
				
				urbanYoungVal = $('input[name=urbanYoungQues]:checked').val();
				if (isNaN(urbanYoungVal)) urbanYoungVal = 0;
				urbanYoungVal = parseInt(urbanYoungVal);
				if(urbanYoungVal > 0) {
					scores["street"]["score"] += urbanYoungVal;
					scores["blackandwhite"]["score"] += urbanYoungVal - 1;
				}
				
				// Fashion
				fashionClassicFitVal = $('input[name=fashionClassicFitQues]:checked').val();
				if (isNaN(fashionClassicFitVal)) fashionClassicFitVal = 0;
				fashionClassicFitVal = parseInt(fashionClassicFitVal);
				if(fashionClassicFitVal > 0) {
					scores["modeling"]["score"] += fashionClassicFitVal;
				}
				
				fashionInternationalVal = $('input[name=fashionInternationalQues]:checked').val();
				if (isNaN(fashionInternationalVal)) fashionInternationalVal = 0;
				fashionInternationalVal = parseInt(fashionInternationalVal);
				if(fashionInternationalVal > 0) {
					scores["fashion"]["score"] += fashionInternationalVal;
					scores["portrait"]["score"] += fashionInternationalVal - 1;
				}
				
				fashionSophisticatedVal = $('input[name=fashionSophisticatedQues]:checked').val();
				if (isNaN(fashionSophisticatedVal)) fashionSophisticatedVal = 0;
				fashionSophisticatedVal = parseInt(fashionSophisticatedVal);
				if(fashionSophisticatedVal > 0) {
					scores["portrait"]["score"] += fashionSophisticatedVal;
				}
				
				fashionWesternVal = $('input[name=fashionWesternQues]:checked').val();
				if (isNaN(fashionWesternVal)) fashionWesternVal = 0;
				fashionWesternVal = parseInt(fashionWesternVal);
				if(fashionWesternVal > 0) {
					scores["portrait"]["score"] += fashionWesternVal;
					scores["sunriseandsunset"]["score"] += fashionWesternVal - 1;
					scores["fashion"]["score"] += fashionWesternVal - 2;
				}
				
				// Travel
				travelAirBallonVal = $('input[name=travelAirBallonQues]:checked').val();
				if(isNaN(travelAirBallonVal)) travelAirBallonVal = 0;
				travelAirBallonVal = parseInt(travelAirBallonVal);
				if(travelAirBallonVal > 0) {
					scores["adventure"]["score"] += travelAirBallonVal;
				}
				
				travelAirportVal = $('input[name=travelAirportQues]:checked').val();
				if(isNaN(travelAirportVal)) travelAirportVal = 0;
				travelAirportVal = parseInt(travelAirportVal);
				if(travelAirportVal > 0) {
					scores["street"]["score"] += travelAirportVal;
					scores["motion"]["score"] += travelAirportVal - 1;
				}
				
				travelRoadVal = $('input[name=travelRoadQues]:checked').val();
				if(isNaN(travelRoadVal)) travelRoadVal = 0;
				travelRoadVal = parseInt(travelRoadVal);
				if(travelRoadVal > 0) {
					scores["adventure"]["score"] += travelRoadVal;
					scores["landscape"]["score"] += travelRoadVal - 1;
				}
				
				travelDesertVal = $('input[name=travelDesertQues]:checked').val();
				if(isNaN(travelDesertVal)) travelDesertVal = 0;
				travelDesertVal = parseInt(travelDesertVal);
				if(travelDesertVal > 0) {
					scores["adventure"]["score"] += travelDesertVal;
					scores["abstraction"]["score"] += travelDesertVal - 1;
					scores["landscape"]["score"] += travelDesertVal - 2;
				}
				
				// Food
				foodCupCakeVal = $('input[name=foodCupCakeQues]:checked').val();
				if(isNaN(foodCupCakeVal)) foodCupCakeVal = 0;
				foodCupCakeVal = parseInt(foodCupCakeVal);
				if(foodCupCakeVal > 0) {
					scores["pattern"]["score"] += foodCupCakeVal;
				}
				
				foodMarketVal = $('input[name=foodMarketQues]:checked').val();
				if(isNaN(foodMarketVal)) foodMarketVal = 0;
				foodMarketVal = parseInt(foodMarketVal);
				if(foodMarketVal > 0) {
					scores["street"]["score"] += foodMarketVal;
				}
				
				foodSoupVal = $('input[name=foodSoupQues]:checked').val();
				if(isNaN(foodSoupVal)) foodSoupVal = 0;
				foodSoupVal = parseInt(foodSoupVal);
				if(foodSoupVal > 0) {
					scores["pattern"]["score"] += foodSoupVal;
				}
				
				foodSteakVal = $('input[name=foodSteakQues]:checked').val();
				if(isNaN(foodSteakVal)) foodSteakVal = 0;
				foodSteakVal = parseInt(foodSteakVal);
				if(foodSteakVal > 0) {
					scores["bokeh"]["score"] += foodSteakVal;
				}
				
				// -------<<Hide Layer 4>>-------
				$("#photoQuestions").fadeOut(1600, function() {
					$(this).remove();
					$('html,body').scrollTop(0);	// Scroll to top before loading Layer 4
					
					// -------<<Show Layer 5>>-------
					$("#calculatedType").fadeIn(1600, function() {
						$(this).show();
					});
				});
				
				// Calculate type of photographer
				var finalType,
					typeText;
				
				/*for(var prop in scores) {
				  console.log(prop + ", score: " + scores[prop].score);
				}*/
				
				// Find type with highest score
				var maxScore = 0;
				for(var prop in scores) {
					var currentScore = scores[prop].score;
					var currentTypeText = scores[prop].textType;
					if(currentScore > maxScore) {
						maxScore = currentScore;
						finalType = prop;
						typeText = currentTypeText;
					}
				}
				
				// Failsafe
				if(maxScore === 0) {
					finalType = "life";
					typeText = "general";
				}
				
				//console.log("finalType = " + finalType);
				//console.log("typeText = " + typeText);
				
				var displayType = "<br /><br /><h2>Looks like your into <b>" + typeText + "</b> photography.</h2>";
				$("#calculatedType").append(displayType);	
				
				// Flickr photo feed
				var myFlickrApiKey = config.flickrApiKey;
				var src;
				var url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=YOURAPIKEYHERE&tags=YOURTAGSHERE&safe_search=1&sort=interestingness-desc&per_page=20";
				
				url = url.replace(/YOURAPIKEYHERE/, myFlickrApiKey);
				url = url.replace(/YOURTAGSHERE/, finalType);
				
				$.getJSON(url + "&format=json&jsoncallback=?", function(data){
					$.each(data.photos.photo, function(i, item) {
						src = "http://farm"+ item.farm +".static.flickr.com/"+ item.server +"/"+ item.id +"_"+ item.secret +"_m.jpg";
						
						$("<img/>").attr("src", src).appendTo("#images");
					});
				});
				
				// DigitalRev link
				var digitalRevLink = "http://www.digitalrev.com/search?t=pho&q=SEARCHITEM";
				
				digitalRevLink = digitalRevLink.replace(/SEARCHITEM/, finalType);
				
				var digitalRevInfo = "<br /><br />" +
									 "<p>Discover more " + finalType + " photography on " +
									 "&nbsp&nbsp<a target='blank' href='" + digitalRevLink + "'><img src='images/inspiration/digitalrevlogo.png' alt=''/></a></p>";
				$("#calculatedType").append(digitalRevInfo);
			});
		});
	});
	
}()); // End of IIFE


























