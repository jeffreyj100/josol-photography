'use strict';

(function() {
	// Form validation
	$("#commentsForm").validate({
		rules: {
			firstName: {
				required: true
			},
			lastName: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			subject: {
				required: true
			},
			comment: {
				required: true
			}
		}
	});
	
	// Verify google recaptcha used
	$("#commentsForm").submit(function(event) {
		var recaptcha = $("#g-recaptcha-response").val();
		if(recaptcha === "") {
			event.preventDefault();
			alert("Please check the recaptcha before submitting.");
		}
	});
}());